import java.io.StringWriter;


public class Shapes {

    public final String SEPARATOR=",";
    private String area;
    StringWriter out;

    public Shapes(StringWriter out) {
        this.out=out;
    }

    public String area(String shapeType, String sides){
        ShapeFactory shapeFactory=new ShapeFactory();
        Shape shape=shapeFactory.createShape(shapeType);
        int area = shape.area(parseSides(sides));
        this.area= String.valueOf(area);
        out.write(this.area);
        out.write("\n");
        return this.area;
    }

    public int[] parseSides(String sides){
//        Arrays.asList(sides.split(SEPARATOR)).stream().
//                map(Double::parseDouble).collect(Collectors.toList());
        int sidesCount = sides.split(SEPARATOR).length;
        int []sidesValues=new int[sidesCount];
        int index = 0;
        for (String side : sides.split(SEPARATOR) ) {
            sidesValues[index]= Integer.parseInt(side);
            index++;
        }
        return sidesValues;
    }
}
