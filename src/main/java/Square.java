public class Square implements Shape {


    public int area(int... sides) {
        int side=sides[0];
        return side*side;
    }
}
