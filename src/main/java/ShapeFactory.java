public class ShapeFactory {


    public Shape createShape(String shapeType){

        if(shapeType.equalsIgnoreCase("TRIANGLE")){
            return new Triangle();

        } else if(shapeType.equalsIgnoreCase("RECTANGLE")){
            return new Rectangle();

        } else if(shapeType.equalsIgnoreCase("SQUARE")){
            return new Square();
        }

       return null;

    }
}
