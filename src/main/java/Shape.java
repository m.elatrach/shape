public interface Shape {

    public int area(int... sides);

}
